package cn.nigel.boot.server.modules.system.service.food.impl;

import cn.nigel.boot.server.modules.system.dal.dataobject.food.FoodDO;
import cn.nigel.boot.server.modules.system.dal.mysql.food.FoodMapper;
import cn.nigel.boot.server.modules.system.service.food.FoodService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class FoodServiceImpl implements FoodService {

    @Resource
    private FoodMapper foodMapper;

    @Override
    public List<FoodDO> getList() {
        return foodMapper.selectList();
    }

    @Override
    public void create(FoodDO foodDO) {
        foodMapper.insert(foodDO);
    }

    @Override
    public void update(FoodDO foodDO) {
        foodMapper.updateById(foodDO);
    }

    @Override
    public void delete(Long id) {
        foodMapper.deleteById(id);
    }
}
