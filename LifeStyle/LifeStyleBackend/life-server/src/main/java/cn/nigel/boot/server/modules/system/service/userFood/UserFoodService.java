package cn.nigel.boot.server.modules.system.service.userFood;

import cn.nigel.boot.server.modules.system.dal.dataobject.userFood.UserFoodDO;

import java.util.List;

public interface UserFoodService {
    List<UserFoodDO> getList(Long userId);

    void create(UserFoodDO userFoodDO);

    void update(UserFoodDO userFoodDO);

    void delete(Long id);
}
