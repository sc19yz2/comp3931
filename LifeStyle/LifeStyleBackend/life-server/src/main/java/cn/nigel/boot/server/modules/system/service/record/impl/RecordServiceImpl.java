package cn.nigel.boot.server.modules.system.service.record.impl;

import cn.nigel.boot.server.modules.system.dal.dataobject.record.RecordDO;
import cn.nigel.boot.server.modules.system.dal.mysql.record.RecordMapper;
import cn.nigel.boot.server.modules.system.service.record.RecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class RecordServiceImpl implements RecordService {

    @Resource
    private RecordMapper recordMapper;

    @Override
    public List<RecordDO> getList(Long userId) {
        return recordMapper.selectListByUserId(userId);
    }

    @Override
    public void create(RecordDO recordDO) {
        recordMapper.insert(recordDO);
    }

    @Override
    public void update(RecordDO recordDO) {
        recordMapper.updateById(recordDO);
    }

    @Override
    public void delete(Long id) {
        recordMapper.deleteById(id);
    }
}
