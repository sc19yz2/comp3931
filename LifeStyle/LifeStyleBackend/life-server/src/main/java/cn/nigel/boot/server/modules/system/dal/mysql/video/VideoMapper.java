package cn.nigel.boot.server.modules.system.dal.mysql.video;

import cn.nigel.boot.framework.mybatis.core.mapper.BaseMapperX;
import cn.nigel.boot.server.modules.system.dal.dataobject.record.RecordDO;
import cn.nigel.boot.server.modules.system.dal.dataobject.video.VideoDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VideoMapper extends BaseMapperX<VideoDO> {

}
