package cn.nigel.boot.server.modules.system.dal.mysql.user;

import cn.nigel.boot.framework.mybatis.core.mapper.BaseMapperX;
import cn.nigel.boot.framework.mybatis.core.query.QueryWrapperX;
import cn.nigel.boot.server.modules.system.dal.dataobject.user.UserDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapperX<UserDO> {
    default UserDO getByName(String name) {
        return selectOne(new QueryWrapperX<UserDO>()
                .eqIfPresent("name", name));
    }
}
