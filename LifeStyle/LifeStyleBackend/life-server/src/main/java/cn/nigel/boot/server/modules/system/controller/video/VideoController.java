package cn.nigel.boot.server.modules.system.controller.video;

import cn.nigel.boot.framework.common.pojo.CommonResult;
import cn.nigel.boot.server.modules.system.dal.dataobject.food.FoodDO;
import cn.nigel.boot.server.modules.system.dal.dataobject.video.VideoDO;
import cn.nigel.boot.server.modules.system.service.video.VideoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.nigel.boot.framework.common.pojo.CommonResult.success;

@RestController
@RequestMapping("/system/video")
public class VideoController {

    @Resource
    private VideoService videoService;

    @GetMapping("/list")
    public CommonResult<List<VideoDO>> getList() {
        return success(videoService.getList());
    }

    @PostMapping("/create")
    public CommonResult<Long> create(@RequestBody VideoDO foodDO) {
        videoService.create(foodDO);
        return success(0L, "Create Successful");
    }

    @PutMapping("update")
    public CommonResult<Boolean> update(@RequestBody VideoDO reqVO) {
        videoService.update(reqVO);
        return success(true, "Update Successful");
    }

    @DeleteMapping("delete")
    public CommonResult<Boolean> delete(@RequestParam("id") Long id) {
        videoService.delete(id);
        return success(true, "Delete Successful");
    }

}
