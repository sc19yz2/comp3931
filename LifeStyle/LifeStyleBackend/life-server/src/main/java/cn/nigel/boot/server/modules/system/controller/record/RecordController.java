package cn.nigel.boot.server.modules.system.controller.record;

import cn.nigel.boot.framework.common.pojo.CommonResult;
import cn.nigel.boot.server.modules.system.dal.dataobject.food.FoodDO;
import cn.nigel.boot.server.modules.system.dal.dataobject.record.RecordDO;
import cn.nigel.boot.server.modules.system.service.record.RecordService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.nigel.boot.framework.common.pojo.CommonResult.success;

@RestController
@RequestMapping("/system/record")
public class RecordController {

    @Resource
    private RecordService recordService;

    @GetMapping("/list")
    public CommonResult<List<RecordDO>> getList(@RequestParam("userId") Long userId) {
        return success(recordService.getList(userId));
    }

    @PostMapping("/create")
    public CommonResult<Long> create(@RequestBody RecordDO reqVO) {
        recordService.create(reqVO);
        return success(0L, "Create Successful");
    }

    @PutMapping("update")
    public CommonResult<Boolean> update(@RequestBody RecordDO reqVO) {
        recordService.update(reqVO);
        return success(true, "Update Successful");
    }

    @DeleteMapping("delete")
    public CommonResult<Boolean> delete(@RequestParam("id") Long id) {
        recordService.delete(id);
        return success(true, "Delete Successful");
    }

}
