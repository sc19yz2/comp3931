package cn.nigel.boot.server.modules.system.service.video.impl;

import cn.nigel.boot.server.modules.system.dal.dataobject.video.VideoDO;
import cn.nigel.boot.server.modules.system.dal.mysql.video.VideoMapper;
import cn.nigel.boot.server.modules.system.service.video.VideoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class VideoServiceImpl implements VideoService {

    @Resource
    private VideoMapper videoMapper;

    @Override
    public List<VideoDO> getList() {
        return videoMapper.selectList();
    }

    @Override
    public void create(VideoDO videoDO) {
        videoMapper.insert(videoDO);
    }

    @Override
    public void update(VideoDO videoDO) {
        videoMapper.updateById(videoDO);
    }

    @Override
    public void delete(Long id) {
        videoMapper.deleteById(id);
    }
}
