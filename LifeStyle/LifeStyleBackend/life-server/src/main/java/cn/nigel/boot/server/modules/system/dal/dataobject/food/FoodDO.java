package cn.nigel.boot.server.modules.system.dal.dataobject.food;

import cn.nigel.boot.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@TableName("sys_food")
@Data
@EqualsAndHashCode(callSuper = true)
public class FoodDO extends BaseDO {

    @TableId
    private Long id;

    private String name;

    private Integer calorie;

}
