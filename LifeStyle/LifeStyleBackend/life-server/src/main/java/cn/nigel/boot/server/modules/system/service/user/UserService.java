package cn.nigel.boot.server.modules.system.service.user;

import cn.nigel.boot.server.modules.system.controller.user.UserData;
import cn.nigel.boot.server.modules.system.dal.dataobject.user.UserDO;

public interface UserService {
    Long create(UserDO userDO);

    UserDO getDetail(Long id);

    void update(UserDO userDO);

    UserDO login(String name, String password);

    UserData getData(Long id);
}
