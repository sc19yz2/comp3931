package cn.nigel.boot.server.modules.system.service.record;

import cn.nigel.boot.server.modules.system.dal.dataobject.record.RecordDO;

import java.util.List;

public interface RecordService {
    List<RecordDO> getList(Long userId);

    void create(RecordDO recordDO);

    void update(RecordDO recordDO);

    void delete(Long id);
}
