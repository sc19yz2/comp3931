package cn.nigel.boot.server.modules.system.dal.dataobject.userFood;

import cn.nigel.boot.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@TableName("sys_user_food")
@Data
@EqualsAndHashCode(callSuper = true)
public class UserFoodDO extends BaseDO {

    @TableId
    private Long id;

    private Long userId;

    private Long foodId;

    private Integer number;

    private String info;

    private Integer type;

    private Integer total;

}
