package cn.nigel.boot.server.modules.system.controller.user;

import cn.nigel.boot.framework.common.pojo.CommonResult;
import cn.nigel.boot.server.modules.system.dal.dataobject.user.UserDO;
import cn.nigel.boot.server.modules.system.service.user.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

import static cn.nigel.boot.framework.common.pojo.CommonResult.error;
import static cn.nigel.boot.framework.common.pojo.CommonResult.success;

@RestController
@RequestMapping("/system/user")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("/signIn")
    public CommonResult<Long> create(@RequestBody UserDO userDO) {
        Long id = userService.create(userDO);
        if (id == -1) {
            return error(-1, "name is exist");
        }
        return success(id, "SignIn Successful");
    }

    @GetMapping("/getInfo")
    public CommonResult<UserDO> getInfo(@RequestParam("id") Long id) {
        final UserDO userDO = userService.getDetail(id);
        return success(userDO);
    }

    @GetMapping("/login")
    public CommonResult<UserDO> login(@RequestParam("name") String name, @RequestParam("password") String password) {
        UserDO userDO = userService.login(name, password);
        if (userDO == null) {
            return error(-1, "login error");
        }
        return success(userDO, "login success");
    }

    @PutMapping("/update")
    public CommonResult<Integer> update(@Valid @RequestBody UserDO userDO) {
        userService.update(userDO);
        return success(1);
    }

    @GetMapping("/getData")
    public CommonResult<UserData> getData(@RequestParam("id") Long id) {
        final UserData userData = userService.getData(id);
        return success(userData);
    }

}
