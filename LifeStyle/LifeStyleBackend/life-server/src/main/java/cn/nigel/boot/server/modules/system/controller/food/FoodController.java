package cn.nigel.boot.server.modules.system.controller.food;

import cn.nigel.boot.framework.common.pojo.CommonResult;
import cn.nigel.boot.server.modules.system.dal.dataobject.food.FoodDO;
import cn.nigel.boot.server.modules.system.service.food.FoodService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.nigel.boot.framework.common.pojo.CommonResult.success;

@RestController
@RequestMapping("/system/food")
public class FoodController {

    @Resource
    private FoodService foodService;

    @GetMapping("/list")
    public CommonResult<List<FoodDO>> getList() {
        return success(foodService.getList());
    }

    @PostMapping("/create")
    public CommonResult<Long> create(@RequestBody FoodDO foodDO) {
        foodService.create(foodDO);
        return success(0L, "Create Successful");
    }

    @PutMapping("update")
    public CommonResult<Boolean> update(@RequestBody FoodDO reqVO) {
        foodService.update(reqVO);
        return success(true, "Update Successful");
    }

    @DeleteMapping("delete")
    public CommonResult<Boolean> delete(@RequestParam("id") Long id) {
        foodService.delete(id);
        return success(true, "Delete Successful");
    }

}
