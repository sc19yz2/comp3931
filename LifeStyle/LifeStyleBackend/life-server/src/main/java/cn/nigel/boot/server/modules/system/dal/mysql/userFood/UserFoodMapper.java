package cn.nigel.boot.server.modules.system.dal.mysql.userFood;

import cn.nigel.boot.framework.mybatis.core.mapper.BaseMapperX;
import cn.nigel.boot.framework.mybatis.core.query.QueryWrapperX;
import cn.nigel.boot.server.modules.system.dal.dataobject.userFood.UserFoodDO;
import org.apache.ibatis.annotations.Mapper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Mapper
public interface UserFoodMapper extends BaseMapperX<UserFoodDO> {

    default List<UserFoodDO> selectListToday(Long userId) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String today = dateFormat.format(new Date());
        return selectList(new QueryWrapperX<UserFoodDO>()
                .eqIfPresent("user_id", userId)
                .betweenIfPresent("create_time", today + " 00:00:00", today + " 23:59:59"));
    }
}
