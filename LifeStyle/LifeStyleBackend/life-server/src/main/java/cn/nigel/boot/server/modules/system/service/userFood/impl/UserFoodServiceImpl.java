package cn.nigel.boot.server.modules.system.service.userFood.impl;

import cn.nigel.boot.server.modules.system.dal.dataobject.userFood.UserFoodDO;
import cn.nigel.boot.server.modules.system.dal.mysql.userFood.UserFoodMapper;
import cn.nigel.boot.server.modules.system.service.userFood.UserFoodService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class UserFoodServiceImpl implements UserFoodService {

    @Resource
    private UserFoodMapper userFoodMapper;

    @Override
    public List<UserFoodDO> getList(Long userId) {
        return userFoodMapper.selectListToday(userId);
    }

    @Override
    public void create(UserFoodDO userFoodDO) {
        userFoodMapper.insert(userFoodDO);
    }

    @Override
    public void update(UserFoodDO userFoodDO) {
        userFoodMapper.updateById(userFoodDO);
    }

    @Override
    public void delete(Long id) {
        userFoodMapper.deleteById(id);
    }
}
