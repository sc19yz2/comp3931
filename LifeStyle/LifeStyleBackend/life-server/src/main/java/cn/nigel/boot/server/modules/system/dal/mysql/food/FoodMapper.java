package cn.nigel.boot.server.modules.system.dal.mysql.food;

import cn.nigel.boot.framework.mybatis.core.mapper.BaseMapperX;
import cn.nigel.boot.server.modules.system.dal.dataobject.food.FoodDO;
import cn.nigel.boot.server.modules.system.dal.dataobject.record.RecordDO;
import cn.nigel.boot.server.modules.system.dal.dataobject.video.VideoDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface FoodMapper extends BaseMapperX<FoodDO> {

}
