package cn.nigel.boot.server.modules.system.controller.userFood;

import cn.nigel.boot.framework.common.pojo.CommonResult;
import cn.nigel.boot.server.modules.system.dal.dataobject.record.RecordDO;
import cn.nigel.boot.server.modules.system.dal.dataobject.userFood.UserFoodDO;
import cn.nigel.boot.server.modules.system.service.userFood.UserFoodService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

import static cn.nigel.boot.framework.common.pojo.CommonResult.success;

@RestController
@RequestMapping("/system/userFood")
public class UserFoodController {

    @Resource
    private UserFoodService userFoodService;

    @GetMapping("/list")
    public CommonResult<List<UserFoodDO>> getList(@RequestParam("userId") Long userId) {
        return success(userFoodService.getList(userId));
    }

    @PostMapping("/create")
    public CommonResult<Long> create(@RequestBody UserFoodDO reqVO) {
        userFoodService.create(reqVO);
        return success(0L, "Create Successful");
    }

    @PutMapping("update")
    public CommonResult<Boolean> update(@RequestBody UserFoodDO reqVO) {
        userFoodService.update(reqVO);
        return success(true, "Update Successful");
    }

    @DeleteMapping("delete")
    public CommonResult<Boolean> delete(@RequestParam("id") Long id) {
        userFoodService.delete(id);
        return success(true, "Delete Successful");
    }

}
