package cn.nigel.boot.server.modules.system.dal.dataobject.user;

import cn.nigel.boot.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@TableName("sys_user")
@Data
@EqualsAndHashCode(callSuper = true)
public class UserDO extends BaseDO {

    @TableId
    private Long id;

    private String name;

    private String password;

    private Integer target;

    private String weight;

    private String height;

    private Integer age;

    private Integer gender;

    private Integer type0;

    private Integer type1;

}
