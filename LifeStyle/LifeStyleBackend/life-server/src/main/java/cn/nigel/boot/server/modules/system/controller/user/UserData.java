package cn.nigel.boot.server.modules.system.controller.user;

import lombok.Data;

@Data
public class UserData {

    private Integer target;

    private Integer distance;

    private Integer calorie;

}
