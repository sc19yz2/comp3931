package cn.nigel.boot.server.modules.system.dal.dataobject.record;

import cn.nigel.boot.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@TableName("sys_record")
@Data
@EqualsAndHashCode(callSuper = true)
public class RecordDO extends BaseDO {

    @TableId
    private Long id;

    private Long userId;

    private String locations;

    private BigDecimal distance;

    private String duration;

}
