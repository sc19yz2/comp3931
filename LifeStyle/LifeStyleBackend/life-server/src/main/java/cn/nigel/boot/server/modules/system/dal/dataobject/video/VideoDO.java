package cn.nigel.boot.server.modules.system.dal.dataobject.video;

import cn.nigel.boot.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@TableName("sys_video")
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoDO extends BaseDO {

    @TableId
    private Long id;

    private String name;

    private String url;

}
