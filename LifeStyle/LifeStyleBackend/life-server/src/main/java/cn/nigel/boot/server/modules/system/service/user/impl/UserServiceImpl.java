package cn.nigel.boot.server.modules.system.service.user.impl;

import cn.nigel.boot.server.modules.system.controller.user.UserData;
import cn.nigel.boot.server.modules.system.dal.dataobject.record.RecordDO;
import cn.nigel.boot.server.modules.system.dal.dataobject.user.UserDO;
import cn.nigel.boot.server.modules.system.dal.dataobject.userFood.UserFoodDO;
import cn.nigel.boot.server.modules.system.dal.mysql.record.RecordMapper;
import cn.nigel.boot.server.modules.system.dal.mysql.user.UserMapper;
import cn.nigel.boot.server.modules.system.service.user.UserService;
import cn.nigel.boot.server.modules.system.service.userFood.UserFoodService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Resource
    private RecordMapper recordMapper;

    @Resource
    private UserFoodService userFoodService;

    @Override
    public Long create(UserDO userDO) {
        UserDO userDO1 = userMapper.getByName(userDO.getName());
        if (userDO1 != null) {
            return -1L;
        }
        userMapper.insert(userDO);
        return userDO.getId();
    }

    @Override
    public UserDO getDetail(Long id) {
        return userMapper.selectById(id);
    }

    @Override
    public void update(UserDO userDO) {
        userMapper.updateById(userDO);
    }

    @Override
    public UserDO login(String name, String password) {
        UserDO userDO = userMapper.getByName(name);
        if (userDO != null && userDO.getPassword().equals(password)) {
            return userDO;
        }
        return null;
    }

    @Override
    public UserData getData(Long id) {
        UserData userData = new UserData();
        UserDO userDO = getDetail(id);
        userData.setTarget(userDO.getTarget());
        List<RecordDO> recordList = recordMapper.getListByUserId(id);
        BigDecimal distance = new BigDecimal(0);
        for (RecordDO recordDO : recordList) {
            distance = distance.add(recordDO.getDistance());
        }
        userData.setDistance(distance.intValue());
        List<UserFoodDO> userFoodList = userFoodService.getList(id);
        int total = 0;
        for (UserFoodDO userFoodDO : userFoodList) {
            total += userFoodDO.getTotal();
        }
        userData.setCalorie(total);
        return userData;
    }
}
