package cn.nigel.boot.server.modules.system.service.video;

import cn.nigel.boot.server.modules.system.dal.dataobject.video.VideoDO;

import java.util.List;

public interface VideoService {
    List<VideoDO> getList();

    void create(VideoDO videoDO);

    void update(VideoDO videoDO);

    void delete(Long id);
}
