package cn.nigel.boot.server.modules.system.dal.mysql.record;

import cn.nigel.boot.framework.mybatis.core.mapper.BaseMapperX;
import cn.nigel.boot.framework.mybatis.core.query.QueryWrapperX;
import cn.nigel.boot.server.modules.system.dal.dataobject.record.RecordDO;
import org.apache.ibatis.annotations.Mapper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Mapper
public interface RecordMapper extends BaseMapperX<RecordDO> {

    default List<RecordDO> selectListByUserId(Long userId) {
        return selectList(new QueryWrapperX<RecordDO>()
                .eqIfPresent("user_id", userId)
                .orderByDesc("id"));
    }

    default List<RecordDO> getListByUserId(Long userId) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String today = dateFormat.format(new Date());
        return selectList(new QueryWrapperX<RecordDO>()
                .eqIfPresent("user_id", userId)
                .betweenIfPresent("create_time", today + " 00:00:00", today + " 23:59:59"));
    }
}
