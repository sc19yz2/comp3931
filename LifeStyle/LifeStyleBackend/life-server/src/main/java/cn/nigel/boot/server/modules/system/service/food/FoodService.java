package cn.nigel.boot.server.modules.system.service.food;

import cn.nigel.boot.server.modules.system.dal.dataobject.food.FoodDO;

import java.util.List;

public interface FoodService {
    List<FoodDO> getList();

    void create(FoodDO foodDO);

    void update(FoodDO foodDO);

    void delete(Long id);
}
