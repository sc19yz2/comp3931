//
//  AppDelegate.swift
//  LifeStyleiOS
//
//  Created by zy on 2022/4/27.
//

import GoogleMaps
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    static var shared: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    static let baseUrl = "http://18.221.81.173:8080/life-server/api/"

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyCzbDZKLyBG5mAhQs9DOtvXa-phQRTuwCk")

        let defaults = UserDefaults.standard

        window?.backgroundColor = .white
        if defaults.string(forKey: "id") != nil {
            window?.rootViewController = TabBarViewController()
        } else {
            window?.rootViewController = ViewController()
        }
        window?.makeKeyAndVisible()

        return true
    }
}
