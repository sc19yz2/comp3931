//
//  FirstViewController.swift
//  LifeStyleiOS
//

import Kingfisher
import RxCocoa
import RxSwift
import SnapKit
import SwifterSwift
import SwiftHTTP
import UIKit

class FirstViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    lazy var contentView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        self.view.backgroundColor = .white
        view.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        return view
    }()
    
    lazy var startButton: UIButton = {
        let view = UIButton()
        view.setTitle("start", for: .normal)
        view.backgroundColor = .green
        view.cornerRadius = 40
        view.rx.tap.asDriver().drive(onNext: { [weak self] () in
            DispatchQueue.main.async {
                let vc = MapViewController()
                self?.navigationController?.pushViewController(vc, animated: true)
                vc.initLocation()
            }
        }).disposed(by: rx.disposeBag)
        return view
    }()
    
    lazy var recordButton: UIButton = {
        let view = UIButton()
        view.setTitle("record", for: .normal)
        view.backgroundColor = .blue
        view.cornerRadius = 40
        view.rx.tap.asDriver().drive(onNext: { [weak self] () in
            self?.navigationController?.pushViewController(RecordViewController(), animated: true)
        }).disposed(by: rx.disposeBag)
        return view
    }()
    
    lazy var recommendButton: UIButton = {
        let view = UIButton()
        view.setTitle("Recommend", for: .normal)
        view.backgroundColor = .orange
        view.cornerRadius = 5
        view.rx.tap.asDriver().drive(onNext: { () in
            if self.todayLabel.titleForNormal == "" {
                return
            }
            if self.recommendButton.backgroundColor == .gray {
                self.recommendButton.backgroundColor = .orange
                self.todaydButton.backgroundColor = .gray
                self.todayLabel.removeFromSuperview()
                self.contentView.addSubview(self.tableView)
                self.tableView.snp.makeConstraints { make in
                    make.width.equalToSuperview()
                    make.top.equalToSuperview().inset(150)
                    make.bottom.equalToSuperview()
                }
            }
        }).disposed(by: rx.disposeBag)
        return view
    }()
    
    lazy var todaydButton: UIButton = {
        let view = UIButton()
        view.setTitle("Today", for: .normal)
        view.backgroundColor = .gray
        view.cornerRadius = 5
        view.rx.tap.asDriver().drive(onNext: { () in
            if self.todayLabel.titleForNormal == "" {
                return
            }
            if self.todaydButton.backgroundColor == .gray {
                self.recommendButton.backgroundColor = .gray
                self.todaydButton.backgroundColor = .orange
                self.tableView.removeFromSuperview()
                self.contentView.addSubview(self.todayLabel)
                self.todayLabel.snp.makeConstraints { make in
                    make.width.equalToSuperview().inset(50)
                    make.top.equalToSuperview().inset(150)
                }
            }
        }).disposed(by: rx.disposeBag)
        return view
    }()
    
    lazy var todayLabel: UIButton = {
        let view = UIButton()
        view.titleForNormal = ""
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            let webVc = VideoViewController()
            self.navigationController?.pushViewController(webVc, animated: true)
            let urlStr = self.type0 == 0 ? "https://youtu.be/lA-diBuGy6I" : "https://youtu.be/zr08J6wB53Y"
            webVc.load(url: urlStr)
        }).disposed(by: rx.disposeBag)
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView(frame: CGRect(), style: .plain)
        return view
    }()

    var dataList: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home"
        contentView.addSubview(startButton)
        startButton.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.equalToSuperview().inset(100)
            make.width.equalTo(80)
            make.height.equalTo(80)
        }
        contentView.addSubview(recordButton)
        recordButton.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview().inset(100)
            make.size.equalTo(80)
        }
        
        contentView.addSubview(recommendButton)
        recommendButton.snp.makeConstraints { make in
            make.right.equalTo(startButton.snp.right)
            make.top.equalToSuperview().inset(100)
            make.width.equalTo(150)
            make.height.equalTo(30)
        }
        
        contentView.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.top.equalToSuperview().inset(150)
            make.bottom.equalToSuperview()
        }
        
        getVideoList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getInfo()
    }
    
    var type0 = -1
    
    func getVideoList() {
        let decoder = JSONDecoder()
        HTTP.GET(AppDelegate.baseUrl + "system/video/list") { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseVideo.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        let data = resp.data ?? []
                        for index in 0 ..< data.count {
                            self.dataList.add(Video(name: data[index].name, url: data[index].url))
                        }
                        self.tableView.delegate = self
                        self.tableView.dataSource = self
                        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CommonCell")
                        self.tableView.tableFooterView = UIView()
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }
    
    func getInfo() {
        let decoder = JSONDecoder()
        let defaults = UserDefaults.standard
        HTTP.GET(AppDelegate.baseUrl + "system/user/getInfo?id=" + (defaults.string(forKey: "id") ?? "")) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseUser.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        if let type0Data = resp.data?.type0 {
                            self.contentView.addSubview(self.todaydButton)
                            self.todaydButton.snp.makeConstraints { make in
                                make.left.equalTo(self.recordButton.snp.left)
                                make.top.equalToSuperview().inset(100)
                                make.width.equalTo(150)
                                make.height.equalTo(30)
                            }
                            if type0Data == 0 {
                                self.todayLabel.titleForNormal = "8 Best Exercises To Build Muscle At Home"
                            } else {
                                self.todayLabel.titleForNormal = "10 MIN HIGH INTENSITY WORKOUT - burn lots of calories, HIIT / No Equipment I Pamela Reif"
                            }
                            self.type0 = type0Data
                        }
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)
        cell.removeSubviews()
        let iv = UIImageView(frame: CGRect())
        cell.addSubview(iv)
        iv.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().inset(10)
            make.size.equalTo(50)
        }
        let label = UILabel()
        label.font = label.font.withSize(14)
        cell.addSubview(label)
        label.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(80)
            make.top.equalToSuperview().inset(15)
            make.right.equalToSuperview().inset(10)
        }
        let video = dataList[indexPath.row] as? Video
        label.text = video?.name
        iv.kf.setImage(with: URL(string: "https://www.2008php.com/2020_Website_appreciate/2020-05-18/20200518122258.jpg"))
        
        let view = UIView()
        cell.addSubview(view)
        view.backgroundColor = .gray
        view.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(0.5)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(dataList[indexPath.row])
        let webVc = VideoViewController()
        navigationController?.pushViewController(webVc, animated: true)
        webVc.load(url: (dataList[indexPath.row] as? Video)?.url ?? "404")
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = .zero
        }
        if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
            cell.layoutMargins = .zero
        }
    }
}
