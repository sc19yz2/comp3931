//
//  FirstViewController.swift
//  LifeStyleiOS
//

import RxCocoa
import RxSwift
import SnapKit
import SwifterSwift
import SwiftHTTP
import UIKit

class ForthViewController: UIViewController {
    lazy var contentView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        self.view.backgroundColor = .white
        view.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        return view
    }()

    lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(22)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = ""
        return view
    }()

    lazy var weightTextField: UITextField = {
        let view = UITextField()
        view.textAlignment = .center
        view.keyboardType = .numberPad
        view.autocapitalizationType = .none
        view.placeholder = "weight"
        view.borderStyle = .line
        return view
    }()

    lazy var weightLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(18)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = " KG"
        return view
    }()

    lazy var heightTextField: UITextField = {
        let view = UITextField()
        view.textAlignment = .center
        view.keyboardType = .numberPad
        view.autocapitalizationType = .none
        view.placeholder = "height"
        view.borderStyle = .line
        return view
    }()

    lazy var heightLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(18)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = " M"
        return view
    }()

    lazy var ageTextField: UITextField = {
        let view = UITextField()
        view.textAlignment = .center
        view.keyboardType = .numberPad
        view.autocapitalizationType = .none
        view.placeholder = "age"
        view.borderStyle = .line
        return view
    }()

    lazy var ageLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(18)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = " Years old"
        return view
    }()

    lazy var genderLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(18)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = "Gender："
        view.textColor = .black
        return view
    }()

    var gender = -1

    lazy var genderRb0: UIButton = {
        let view = UIButton()
        view.titleForNormal = "Male"
        view.borderWidth = 2
        view.cornerRadius = 2
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            self.genderRb0.titleColorForNormal = .red
            self.genderRb1.titleColorForNormal = .black
            self.gender = 0
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var genderRb1: UIButton = {
        let view = UIButton()
        view.titleForNormal = "Female"
        view.borderWidth = 2
        view.cornerRadius = 2
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            self.genderRb0.titleColorForNormal = .black
            self.genderRb1.titleColorForNormal = .red
            self.gender = 1
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var type0Label: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(18)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = "Goal："
        view.textColor = .black
        return view
    }()

    var type0 = -1

    lazy var type0Rb0: UIButton = {
        let view = UIButton()
        view.titleForNormal = "Muscle"
        view.borderWidth = 2
        view.cornerRadius = 2
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            self.type0Rb0.titleColorForNormal = .red
            self.type0Rb1.titleColorForNormal = .black
            self.type0 = 0
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var type0Rb1: UIButton = {
        let view = UIButton()
        view.titleForNormal = "FatLoss"
        view.borderWidth = 2
        view.cornerRadius = 2
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            self.type0Rb0.titleColorForNormal = .black
            self.type0Rb1.titleColorForNormal = .red
            self.type0 = 1
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var type1Label: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(18)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = "Exercise："
        view.textColor = .black
        return view
    }()

    var type1 = -1

    lazy var type1Rb0: UIButton = {
        let view = UIButton()
        view.titleForNormal = "LittleOrNo"
        view.borderWidth = 2
        view.cornerRadius = 2
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            self.type1Rb0.titleColorForNormal = .red
            self.type1Rb1.titleColorForNormal = .black
            self.type1Rb2.titleColorForNormal = .black
            self.type1Rb3.titleColorForNormal = .black
            self.type1 = 0
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var type1Rb1: UIButton = {
        let view = UIButton()
        view.titleForNormal = "Bit(1-3/week)"
        view.borderWidth = 2
        view.cornerRadius = 2
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            self.type1Rb0.titleColorForNormal = .black
            self.type1Rb1.titleColorForNormal = .red
            self.type1Rb2.titleColorForNormal = .black
            self.type1Rb3.titleColorForNormal = .black
            self.type1 = 1
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var type1Rb2: UIButton = {
        let view = UIButton()
        view.titleForNormal = "Middle(3-6/week)"
        view.borderWidth = 2
        view.cornerRadius = 2
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            self.type1Rb0.titleColorForNormal = .black
            self.type1Rb1.titleColorForNormal = .black
            self.type1Rb2.titleColorForNormal = .red
            self.type1Rb3.titleColorForNormal = .black
            self.type1 = 2
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var type1Rb3: UIButton = {
        let view = UIButton()
        view.titleForNormal = "Lot(1hour/day)"
        view.borderWidth = 2
        view.cornerRadius = 2
        view.titleColorForNormal = .black
        view.rx.tap.asDriver().drive(onNext: { () in
            self.type1Rb0.titleColorForNormal = .black
            self.type1Rb1.titleColorForNormal = .black
            self.type1Rb2.titleColorForNormal = .black
            self.type1Rb3.titleColorForNormal = .red
            self.type1 = 3
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var bmiLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(22)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.textColor = .red
        return view
    }()

    lazy var saveButton: UIButton = {
        let view = UIButton()
        view.setTitle("Save", for: .normal)
        view.backgroundColor = .blue
        view.cornerRadius = 5
        view.rx.tap.asDriver().drive(onNext: { () in
            let weight = self.weightTextField.text ?? ""
            let height = self.heightTextField.text ?? ""
            let age = self.ageTextField.text ?? ""
            if weight == "" || height == "" {
                self.showToast(message: "weight or height can not be null")
                return
            }
            let decoder = JSONDecoder()
            let defaults = UserDefaults.standard
            HTTP.PUT(AppDelegate.baseUrl + "system/user/update", parameters: ["id": defaults.string(forKey: "id") ?? "", "weight": weight, "height": height, "age": age, "gender": self.gender, "type0": self.type0, "type1": self.type1], requestSerializer: JSONParameterSerializer()) { response in
                if let error = response.error {
                    print("got an error: \(error)")
                    return
                }
                do {
                    let resp = try decoder.decode(ResponseInt.self, from: response.data)
                    print("completed: \(resp.code ?? -100)")
                    DispatchQueue.main.async {
                        if resp.code == 0 {
                            self.getInfo()
                        } else {
                            self.showToast(message: resp.msg ?? "unknown")
                        }
                    }
                } catch {
                    print("decode json error: \(error)")
                    DispatchQueue.main.async {
                        self.showToast(message: "decode json error: \(error)")
                    }
                }
            }
        }).disposed(by: rx.disposeBag)
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Profile"

        contentView.addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(10)
        }

        contentView.addSubview(weightTextField)
        weightTextField.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(100)
            make.width.equalTo(180)
        }
        contentView.addSubview(weightLabel)
        weightLabel.snp.makeConstraints { make in
            make.top.equalTo(weightTextField.snp.top).inset(2)
            make.left.equalTo(weightTextField.snp.right)
        }

        contentView.addSubview(heightTextField)
        heightTextField.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(150)
            make.width.equalTo(180)
        }
        contentView.addSubview(heightLabel)
        heightLabel.snp.makeConstraints { make in
            make.top.equalTo(heightTextField.snp.top).inset(2)
            make.left.equalTo(heightTextField.snp.right)
        }

        contentView.addSubview(ageTextField)
        ageTextField.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(200)
            make.width.equalTo(180)
        }
        contentView.addSubview(ageLabel)
        ageLabel.snp.makeConstraints { make in
            make.top.equalTo(ageTextField.snp.top).inset(2)
            make.left.equalTo(ageTextField.snp.right)
        }

        contentView.addSubview(genderRb0)
        genderRb0.snp.makeConstraints { make in
            make.left.equalTo(ageTextField.snp.left)
            make.top.equalToSuperview().inset(250)
            make.width.equalTo(75)
        }
        contentView.addSubview(genderRb1)
        genderRb1.snp.makeConstraints { make in
            make.right.equalTo(ageTextField.snp.right)
            make.top.equalTo(genderRb0.snp.top)
            make.width.equalTo(75)
        }
        contentView.addSubview(genderLabel)
        genderLabel.snp.makeConstraints { make in
            make.right.equalTo(genderRb0.snp.left)
            make.top.equalTo(genderRb0.snp.top).inset(4)
        }

        contentView.addSubview(type0Rb0)
        type0Rb0.snp.makeConstraints { make in
            make.left.equalTo(ageTextField.snp.left)
            make.top.equalToSuperview().inset(300)
            make.width.equalTo(75)
        }
        contentView.addSubview(type0Rb1)
        type0Rb1.snp.makeConstraints { make in
            make.right.equalTo(ageTextField.snp.right)
            make.top.equalTo(type0Rb0.snp.top)
            make.width.equalTo(75)
        }
        contentView.addSubview(type0Label)
        type0Label.snp.makeConstraints { make in
            make.right.equalTo(type0Rb0.snp.left)
            make.top.equalTo(type0Rb0.snp.top).inset(4)
        }

        contentView.addSubview(type1Rb0)
        type1Rb0.snp.makeConstraints { make in
            make.left.equalTo(ageTextField.snp.left)
            make.top.equalToSuperview().inset(350)
            make.width.equalTo(100)
        }
        contentView.addSubview(type1Rb1)
        type1Rb1.snp.makeConstraints { make in
            make.left.equalTo(type1Rb0.snp.right).inset(-10)
            make.top.equalTo(type1Rb0.snp.top)
            make.width.equalTo(130)
        }
        contentView.addSubview(type1Label)
        type1Label.snp.makeConstraints { make in
            make.right.equalTo(type1Rb0.snp.left)
            make.top.equalTo(type1Rb0.snp.top).inset(4)
        }
        contentView.addSubview(type1Rb2)
        type1Rb2.snp.makeConstraints { make in
            make.left.equalTo(ageTextField.snp.left).inset(-50)
            make.top.equalToSuperview().inset(390)
            make.width.equalTo(155)
        }
        contentView.addSubview(type1Rb3)
        type1Rb3.snp.makeConstraints { make in
            make.left.equalTo(type1Rb2.snp.right).inset(-10)
            make.top.equalTo(type1Rb2.snp.top)
            make.width.equalTo(130)
        }

        contentView.addSubview(bmiLabel)
        bmiLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(450)
        }

        contentView.addSubview(saveButton)
        saveButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(500)
            make.width.equalTo(150)
            make.height.equalTo(50)
        }

        getInfo()
    }

    func getInfo() {
        let decoder = JSONDecoder()
        let defaults = UserDefaults.standard
        HTTP.GET(AppDelegate.baseUrl + "system/user/getInfo?id=" + (defaults.string(forKey: "id") ?? "")) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseUser.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        self.nameLabel.text = resp.data?.name
                        self.weightTextField.text = resp.data?.weight
                        self.heightTextField.text = resp.data?.height
                        self.ageTextField.text = resp.data?.age?.string

                        if let genderData = resp.data?.gender {
                            if genderData == 0 {
                                self.genderRb0.titleColorForNormal = .red
                                self.genderRb1.titleColorForNormal = .black
                            } else if genderData == 1 {
                                self.genderRb0.titleColorForNormal = .black
                                self.genderRb1.titleColorForNormal = .red
                            }
                            self.gender = genderData
                        }
                        if let type0Data = resp.data?.type0 {
                            if type0Data == 0 {
                                self.type0Rb0.titleColorForNormal = .red
                                self.type0Rb1.titleColorForNormal = .black
                            } else if type0Data == 1 {
                                self.type0Rb0.titleColorForNormal = .black
                                self.type0Rb1.titleColorForNormal = .red
                            }
                            self.type0 = type0Data
                        }
                        if let type1Data = resp.data?.type1 {
                            if type1Data == 0 {
                                self.type1Rb0.titleColorForNormal = .red
                                self.type1Rb1.titleColorForNormal = .black
                                self.type1Rb2.titleColorForNormal = .black
                                self.type1Rb3.titleColorForNormal = .black
                            } else if type1Data == 1 {
                                self.type0Rb0.titleColorForNormal = .black
                                self.type0Rb1.titleColorForNormal = .red
                                self.type1Rb2.titleColorForNormal = .black
                                self.type1Rb3.titleColorForNormal = .black
                            } else if type1Data == 2 {
                                self.type0Rb0.titleColorForNormal = .black
                                self.type0Rb1.titleColorForNormal = .black
                                self.type1Rb2.titleColorForNormal = .red
                                self.type1Rb3.titleColorForNormal = .black
                            } else if type1Data == 3 {
                                self.type0Rb0.titleColorForNormal = .black
                                self.type0Rb1.titleColorForNormal = .black
                                self.type1Rb2.titleColorForNormal = .black
                                self.type1Rb3.titleColorForNormal = .red
                            }
                            self.type1 = type1Data
                        }

                        if let weight = resp.data?.weight, let height = resp.data?.height {
                            let bmi = String(format: "%.2f", (Double(weight) ?? 0) / ((Double(height) ?? 0) * (Double(height) ?? 0)))
                            self.bmiLabel.text = "BMI: " + bmi + "  (normal: 18.5-24)"
                        }
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }
}
