//
//  UIRadioButton.swift
//  LifeStyleiOS
//

import UIKit

class UIRadioButton: UIButton {
    private var ischeck = false

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.layer.borderColor = UIColor(red: 255/255, green: 110/255, blue: 2/255, alpha: 1).cgColor

        self.layer.borderWidth = 1

        self.layer.cornerRadius = 8

        self.setTextColor(color: UIColor(red: 255/255, green: 110/255, blue: 2/255, alpha: 1))

        self.setTextFont(size: 14)
    }

    func setTextColor(color: UIColor) {
        setTextColor(color: color)
    }

    func setTextFont(size: CGFloat) {
        self.titleLabel!.font = UIFont.systemFont(ofSize: size)
    }

    func setText(title: String) {
        titleForNormal = title
    }
    
    func tapped(button: UIButton) {
        if self.isCheck() {
            self.setCheck(isCheck: false)
        } else {
            self.setCheck(isCheck: true)
        }
    }

    func setCheck(isCheck: Bool) {
        self.ischeck = isCheck
        if isCheck {
            self.backgroundColor = UIColor(red: 255/255, green: 110/255, blue: 2/255, alpha: 1)
            self.setTextColor(color: UIColor.white)
        } else {
            self.backgroundColor = UIColor.white
            self.setTextColor(color: UIColor(red: 255/255, green: 110/255, blue: 2/255, alpha: 1))
        }
    }

    func isCheck() -> Bool {
        return self.ischeck
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
