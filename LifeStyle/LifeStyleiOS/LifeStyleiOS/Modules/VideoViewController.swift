//
//  VideoViewController.swift
//  LifeStyleiOS
//

import UIKit
import WebKit

class VideoViewController: UIViewController {
    lazy var contentView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        self.view.backgroundColor = .white
        view.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        return view
    }()
    
    lazy var webView: WKWebView = {
        let view = WKWebView()
        view.backgroundColor = .clear
        view.allowsBackForwardNavigationGestures = true
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "VideoDetail"
        
        contentView.addSubview(webView)
        webView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func load(url: String) {
        if let url = url.url {
            webView.load(URLRequest(url: url))
        } else {
            webView.loadHTMLString("<font size=\"12\">\(url)</font>", baseURL: nil)
        }
    }
}
