//
//  MenuViewController.swift
//  LifeStyleiOS
//

import SwiftHTTP
import UIKit

class RecordViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    lazy var contentView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        self.view.backgroundColor = .white
        view.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView(frame: CGRect(), style: .plain)
        return view
    }()

    var dataList: NSMutableArray = []
    let selectedList: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Record"
        
        contentView.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CommonCell")
        tableView.tableFooterView = UIView()
        
        getList()
    }
    
    func getList() {
        let decoder = JSONDecoder()
        let defaults = UserDefaults.standard
        HTTP.GET(AppDelegate.baseUrl + "system/record/list?userId=" + (defaults.string(forKey: "id") ?? "")) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseRecord.self, from: response.data)
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        self.dataList.removeAllObjects()
                        let data = resp.data ?? []
                        for index in 0 ..< data.count {
                            print(data[index])
                            self.dataList.add(Record(locations: data[index].locations, duration: data[index].duration, createTime: (data[index].createTime ?? 0) / 1000, distance: data[index].distance))
                        }
                        self.tableView.reloadData()
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)
        cell.removeSubviews()
        let label = UILabel()
        label.font = label.font.withSize(14)
        cell.addSubview(label)
        label.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
            make.right.equalToSuperview().inset(10)
        }
        
        let label2 = UILabel()
        label2.font = label.font.withSize(12)
        cell.addSubview(label2)
        label2.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(10)
            make.top.equalTo(label.snp.bottom)
            make.right.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(10)
        }
        
        let label3 = UILabel()
        label3.font = label.font.withSize(12)
        cell.addSubview(label3)
        label3.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(10)
            make.top.equalTo(label.snp.top)
        }
        
        let record = dataList[indexPath.row] as? Record
        label.text = "Distance: " + (record?.distance?.string ?? "")
        label2.text = "Duration: " + (record?.duration ?? "")
        label3.text = timeIntervalChangeToTimeStr(timeInterval: record?.createTime ?? 0)
        
        let view = UIView()
        cell.addSubview(view)
        view.backgroundColor = .gray
        view.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(0.5)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(dataList[indexPath.row])
        let vc = MapViewController()
        navigationController?.pushViewController(vc, animated: true)
        vc.setRecord(record: dataList[indexPath.row] as! Record)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = .zero
        }
        if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
            cell.layoutMargins = .zero
        }
    }
    
    func timeIntervalChangeToTimeStr(timeInterval: Int, _ dateFormat: String? = "yyyy-MM-dd HH:mm:ss") -> String {
        let date = Date(timeIntervalSince1970: timeInterval.double)
        let formatter = DateFormatter()
        if dateFormat == nil {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        } else {
            formatter.dateFormat = dateFormat
        }
        return formatter.string(from: date as Date)
    }
}
