//
//  TabBarViewController.swift
//  LifeStyleiOS
//

import UIKit

class TabBarViewController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        creatSubViewControllers()
        tabBar.backgroundColor = .white
    }
    
    func creatSubViewControllers() {
        let v1 = UINavigationController(rootViewController: FirstViewController())
        v1.tabBarItem = getTabBar(title: "Home")
        
        let v2 = UINavigationController(rootViewController: SecondViewController())
        v2.tabBarItem = getTabBar(title: "Calorie")
        
        let v3 = UINavigationController(rootViewController: ThirdViewController())
        v3.tabBarItem = getTabBar(title: "Plan")
        
        let v4 = UINavigationController(rootViewController: ForthViewController())
        v4.tabBarItem = getTabBar(title: "Profile")
        
        let tabArray = [v1, v2, v3, v4]
        viewControllers = tabArray
    }
    
    func getTabBar(title: String) -> UITabBarItem {
        let item = UITabBarItem(title: title, image: nil, selectedImage: nil)
        item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
        item.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        item.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        return item
    }
}
