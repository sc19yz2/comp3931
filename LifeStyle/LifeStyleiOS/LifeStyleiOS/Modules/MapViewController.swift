//
//  MapViewController.swift
//  LifeStyleiOS
//

import CoreLocation
import GoogleMaps
import SwifterSwift
import SwiftHTTP
import UIKit

class MapViewController: UIViewController, CLLocationManagerDelegate {
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    lazy var infoLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(15)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = ""
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        return view
    }()

    lazy var stopButton: UIButton = {
        let view = UIButton()
        view.setTitle("Stop", for: .normal)
        view.backgroundColor = .red
        view.cornerRadius = 10
        let defaults = UserDefaults.standard
        view.rx.tap.asDriver().drive(onNext: {
            let decoder = JSONDecoder()
            HTTP.POST(AppDelegate.baseUrl + "system/record/create", parameters: ["userId": defaults.string(forKey: "id") ?? "", "locations": self.locationsStr, "distance": self.total, "duration": self.dateInterval()], requestSerializer: JSONParameterSerializer()) { response in
                if let error = response.error {
                    print("got an error: \(error)")
                    return
                }
                do {
                    let resp = try decoder.decode(ResponseInt.self, from: response.data)
                    print("completed: \(resp.code ?? -100)")
                    DispatchQueue.main.async {
                        self.showToast(message: resp.msg ?? "unknown")
                        if resp.code == 0 {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                } catch {
                    print("decode json error: \(error)")
                    DispatchQueue.main.async {
                        self.showToast(message: "decode json error: \(error)")
                    }
                }
            }
        }).disposed(by: rx.disposeBag)
        return view
    }()

    var mapView: GMSMapView?
    var lastLocation: CLLocationCoordinate2D?

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")

        if mapView == nil {
            let camera = GMSCameraPosition.camera(withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 16.0)
            mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
            mapView?.accessibilityElementsHidden = false
            mapView?.isMyLocationEnabled = true
            view.addSubview(mapView!)

            view.addSubview(stopButton)
            stopButton.snp.makeConstraints { make in
                make.top.left.equalTo(self.view.safeAreaLayoutGuide).inset(10)
                make.size.equalTo(50)
            }
            view.addSubview(infoLabel)
            infoLabel.snp.makeConstraints { make in
                make.top.equalTo(stopButton.snp.top)
                make.left.equalToSuperview().inset(70)
                make.right.equalToSuperview().inset(10)
            }
            startTime = getCurrentTime()
            infoLabel.text = "StartTime: " + startTime
            locationsStr += (locValue.latitude.string + "," + locValue.longitude.string + ";")
        } else {
            mapView?.animate(toLocation: CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude))

            let path = GMSMutablePath()
            path.addLatitude(lastLocation!.latitude, longitude: lastLocation!.longitude)
            path.addLatitude(locValue.latitude, longitude: locValue.longitude)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 3.0
            polyline.geodesic = true
            polyline.map = mapView

            let distance = GMSGeometryDistance(lastLocation!, locValue)
            if distance > 0 {
                total += Float(distance)
                infoLabel.text = "StartTime: " + startTime + ", Distance: " + total.string
                locationsStr += (locValue.latitude.string + "," + locValue.longitude.string + ";")
            }
        }

        lastLocation = locValue
    }

    var locationsStr: String = ""
    var total: Float = 0
    var startTime = ""

    public func dateInterval() -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let date1 = dateFormatter.date(from: startTime),
              let date2 = dateFormatter.date(from: getCurrentTime())
        else {
            return ""
        }
        let components = NSCalendar.current.dateComponents([.second], from: date1, to: date2)
        return transToHourMinSec(allTime: components.second!)
    }

    func transToHourMinSec(allTime: Int) -> String {
        var hours = 0
        var minutes = 0
        var seconds = 0
        var hoursText = ""
        var minutesText = ""
        var secondsText = ""

        hours = allTime / 3600
        hoursText = hours > 9 ? "\(hours)" : "0\(hours)"

        minutes = allTime % 3600 / 60
        minutesText = minutes > 9 ? "\(minutes)" : "0\(minutes)"

        seconds = allTime % 3600 % 60
        secondsText = seconds > 9 ? "\(seconds)" : "0\(seconds)"

        return "\(hoursText):\(minutesText):\(secondsText)"
    }

    func getCurrentTime() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = formatter.string(from: Date())
        return dateTime
    }

    func initLocation() {
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }

    func setRecord(record: Record) {
        let paths = record.locations!.components(separatedBy: ";").filter { item in
            item != ""
        }
        var locStart = paths[0].components(separatedBy: ",")

        let camera = GMSCameraPosition.camera(withLatitude: Double(locStart[0])!, longitude: Double(locStart[1])!, zoom: 16.0)
        mapView = GMSMapView.map(withFrame: view.frame, camera: camera)
        mapView?.accessibilityElementsHidden = false
        mapView?.isMyLocationEnabled = true
        view.addSubview(mapView!)

        for index in 1 ..< paths.count {
            let locEnd = paths[index].components(separatedBy: ",")
            let path = GMSMutablePath()
            path.addLatitude(Double(locStart[0])!, longitude: Double(locStart[1])!)
            path.addLatitude(Double(locEnd[0])!, longitude: Double(locEnd[1])!)
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 3.0
            polyline.geodesic = true
            polyline.map = mapView
            locStart = locEnd
        }

        view.addSubview(infoLabel)
        infoLabel.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide).inset(10)
            make.left.equalToSuperview().inset(10)
            make.right.equalToSuperview().inset(10)
        }
        infoLabel.text = timeIntervalChangeToTimeStr(timeInterval: record.createTime!) + ", Distance: " + (record.distance?.string ?? "") + "Duration: " + (record.duration ?? "")
    }

    func timeIntervalChangeToTimeStr(timeInterval: Int, _ dateFormat: String? = "yyyy-MM-dd HH:mm:ss") -> String {
        let date = Date(timeIntervalSince1970: timeInterval.double)
        let formatter = DateFormatter()
        if dateFormat == nil {
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        } else {
            formatter.dateFormat = dateFormat
        }
        return formatter.string(from: date as Date)
    }
}
