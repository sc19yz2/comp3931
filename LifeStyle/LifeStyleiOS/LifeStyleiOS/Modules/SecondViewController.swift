//
//  FirstViewController.swift
//  LifeStyleiOS
//

import SwiftHTTP
import UIKit

class SecondViewController: UIViewController {
    lazy var contentView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        self.view.backgroundColor = .white
        view.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        return view
    }()

    lazy var dataLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(22)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        return view
    }()

    lazy var suggestionLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(22)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.numberOfLines = 0
        view.lineBreakMode = .byWordWrapping
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Calorie"

        contentView.addSubview(dataLabel)
        dataLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }

//        contentView.addSubview(suggestionLabel)
//        suggestionLabel.snp.makeConstraints { make in
//            make.left.right.equalToSuperview().inset(10)
//            make.top.equalToSuperview().inset(280)
//        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let defaults = UserDefaults.standard
        let decoder = JSONDecoder()
        HTTP.GET(AppDelegate.baseUrl + "system/user/getData?id=" + (defaults.string(forKey: "id") ?? "")) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseData.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        self.dataLabel.text = "Today Distance: " + (resp.data?.distance?.string ?? "") + "(target:" + (resp.data?.target?.string ?? "") + ")" + "\n\nToday Calorie: " + (resp.data?.calorie?.string ?? "") + " KCAL"

//                        if let target = resp.data?.target, let distance = resp.data?.distance, let calorie = resp.data?.calorie {
//                            var suggstion = ""
//                            if target > distance {
//                                suggstion = "Please keep running"
//                            } else {
//                                suggstion = "Target completion"
//                            }
//                            if calorie == 0 {
//                                suggstion += "\n\nPlease eat on time"
//                            } else if Double(calorie) > Double(distance) * 0.1 {
//                                suggstion += "\n\noday's calorie intake is excessive. Please start exercising"
//                            } else {
//                                suggstion += "\n\nToday's exercise is very good. I've burned calories"
//                            }
//                            self.suggestionLabel.text = suggstion
//                        }
                        self.getInfo()
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }

    func getInfo() {
        let decoder = JSONDecoder()
        let defaults = UserDefaults.standard
        HTTP.GET(AppDelegate.baseUrl + "system/user/getInfo?id=" + (defaults.string(forKey: "id") ?? "")) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseUser.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        let weight = Double(resp.data?.weight ?? "0")!
                        let height = Double(resp.data?.height ?? "0")!
                        let age = resp.data?.age ?? 0
                        let genderData = resp.data?.gender ?? 0
                        let type0Data = resp.data?.type0 ?? 0
                        let type1Data = resp.data?.type1 ?? 0

                        var result0 = 9.99 * weight + 625 * height - 4.92 * Double(age)
                        if genderData == 0 {
                            result0 = result0 + 5
                        } else {
                            result0 = result0 - 161
                        }
                        if type1Data == 0 {
                            result0 = result0 * 1.2
                        } else if type1Data == 1 {
                            result0 = result0 * 1.375
                        } else if type1Data == 2 {
                            result0 = result0 * 1.725
                        } else {
                            result0 = result0 * 1.9
                        }
                        if type0Data == 0 {
                            result0 = result0 + 300
                        }

                        self.dataLabel.text = self.dataLabel.text! + "\n\nSuggest Intake:" + String(format: "%.2f", result0) + " KCAL"

                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }
}
