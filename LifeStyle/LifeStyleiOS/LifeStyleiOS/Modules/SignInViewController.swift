//
//  ViewController.swift
//  LifeStyleiOS
//

import NSObject_Rx
import RxCocoa
import RxSwift
import SnapKit
import SwiftHTTP
import UIKit

class SignInViewController: UIViewController {
    lazy var contentView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        self.view.backgroundColor = .white
        view.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        return view
    }()

    lazy var stackView: UIStackView = {
        let subviews: [UIView] = []
        let view = UIStackView(arrangedSubviews: subviews)
        view.spacing = 20
        view.axis = .vertical
        self.contentView.addSubview(view)
        view.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(200)
            make.width.equalTo(200)
        }
        return view
    }()

    lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(22)
        view.numberOfLines = 0
        view.textAlignment = .center
        view.text = "Sign in"
        return view
    }()

    lazy var loginTextField: UITextField = {
        let view = UITextField()
        view.textAlignment = .center
        view.keyboardType = .emailAddress
        view.autocapitalizationType = .none
        view.placeholder = "username"
        view.borderStyle = .line
        return view
    }()

    lazy var passwordTextField: UITextField = {
        let view = UITextField()
        view.textAlignment = .center
        view.isSecureTextEntry = true
        view.placeholder = "password"
        view.borderStyle = .line
        return view
    }()

    lazy var signInButton: UIButton = {
        let view = UIButton()
        view.setTitle("sign in", for: .normal)
        view.backgroundColor = .blue
        view.rx.tap.asDriver().drive(onNext: { [weak self] _ in
            let decoder = JSONDecoder()
            HTTP.POST(AppDelegate.baseUrl + "system/user/signIn", parameters: ["name": self?.loginTextField.text ?? "", "password": self?.passwordTextField.text ?? ""], requestSerializer: JSONParameterSerializer()) { response in
                if let error = response.error {
                    print("got an error: \(error)")
                    return
                }
                do {
                    let resp = try decoder.decode(ResponseInt.self, from: response.data)
                    print("completed: \(resp.code ?? -100)")
                    DispatchQueue.main.async {
                        self?.showToast(message: resp.msg ?? "unknown")
                        if resp.code == 0 {
                            self?.dismiss(animated: true)
                        }
                    }
                } catch {
                    print("decode json error: \(error)")
                    DispatchQueue.main.async {
                        self?.showToast(message: "decode json error: \(error)")
                    }
                }
            }
        }).disposed(by: rx.disposeBag)
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(loginTextField)
        stackView.addArrangedSubview(passwordTextField)
        stackView.addArrangedSubview(signInButton)
    }
}
