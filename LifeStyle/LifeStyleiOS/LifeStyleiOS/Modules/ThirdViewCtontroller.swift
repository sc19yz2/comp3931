//
//  FirstViewController.swift
//  LifeStyleiOS
//

import SwiftHTTP
import UIKit

class ThirdViewController: UIViewController {
    lazy var contentView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        view.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        return view
    }()

    lazy var breakfastLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(22)
        view.numberOfLines = 0
        view.textAlignment = .left
        view.text = "Breakfast  "
        return view
    }()

    lazy var breakfastButton: UIButton = {
        let view = UIButton()
        view.setTitle("Select", for: .normal)
        view.cornerRadius = 5
        view.borderWidth = 1
        view.titleColorForNormal = .black
        view.titleLabel?.font = view.titleLabel?.font.withSize(15)
        view.rx.tap.asDriver().drive(onNext: { [weak self] () in
            let vc = MenuViewController()
            self?.navigationController?.pushViewController(vc, animated: true)
            vc.setType(type: 1)
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var breakfastDescLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(16)
        view.numberOfLines = 0
        view.textAlignment = .left
        view.textColor = .gray
        return view
    }()

    lazy var launchLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(22)
        view.numberOfLines = 0
        view.textAlignment = .left
        view.text = "Launch  "
        return view
    }()

    lazy var launchButton: UIButton = {
        let view = UIButton()
        view.setTitle("Select", for: .normal)
        view.cornerRadius = 5
        view.borderWidth = 1
        view.titleColorForNormal = .black
        view.titleLabel?.font = view.titleLabel?.font.withSize(15)
        view.rx.tap.asDriver().drive(onNext: { [weak self] () in
            let vc = MenuViewController()
            self?.navigationController?.pushViewController(vc, animated: true)
            vc.setType(type: 2)
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var launchDescLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(16)
        view.numberOfLines = 0
        view.textAlignment = .left
        view.textColor = .gray
        return view
    }()

    lazy var dinnerLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(22)
        view.numberOfLines = 0
        view.textAlignment = .left
        view.text = "Dinner  "
        return view
    }()

    lazy var dinnerButton: UIButton = {
        let view = UIButton()
        view.setTitle("Select", for: .normal)
        view.cornerRadius = 5
        view.borderWidth = 1
        view.titleColorForNormal = .black
        view.titleLabel?.font = view.titleLabel?.font.withSize(15)
        view.rx.tap.asDriver().drive(onNext: { [weak self] () in
            let vc = MenuViewController()
            self?.navigationController?.pushViewController(vc, animated: true)
            vc.setType(type: 3)
        }).disposed(by: rx.disposeBag)
        return view
    }()

    lazy var dinnerDescLabel: UILabel = {
        let view = UILabel()
        view.font = view.font.withSize(16)
        view.numberOfLines = 0
        view.textAlignment = .left
        view.textColor = .gray
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Plan"
        let item = UIBarButtonItem(title: "menu", style: .plain, target: self, action: #selector(openMenu))
        navigationItem.rightBarButtonItem = item

        contentView.addSubview(breakfastLabel)
        breakfastLabel.snp.makeConstraints { make in
            make.left.top.equalToSuperview().inset(15)
        }
        contentView.addSubview(breakfastButton)
        breakfastButton.snp.makeConstraints { make in
            make.left.equalTo(breakfastLabel.snp.right)
            make.top.equalToSuperview().inset(8)
            make.width.equalTo(100)
        }
        contentView.addSubview(breakfastDescLabel)
        breakfastDescLabel.snp.makeConstraints { make in
            make.top.equalTo(breakfastLabel.snp.bottom)
            make.left.equalToSuperview().inset(15)
        }

        contentView.addSubview(launchLabel)
        launchLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(130)
            make.left.equalTo(breakfastLabel.snp.left)
        }
        contentView.addSubview(launchButton)
        launchButton.snp.makeConstraints { make in
            make.left.equalTo(breakfastLabel.snp.right)
            make.top.equalTo(launchLabel.snp.top)
            make.width.equalTo(100)
        }
        contentView.addSubview(launchDescLabel)
        launchDescLabel.snp.makeConstraints { make in
            make.top.equalTo(launchLabel.snp.bottom)
            make.left.equalToSuperview().inset(15)
        }

        contentView.addSubview(dinnerLabel)
        dinnerLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(249)
            make.left.equalTo(breakfastLabel.snp.left)
        }
        contentView.addSubview(dinnerButton)
        dinnerButton.snp.makeConstraints { make in
            make.left.equalTo(breakfastLabel.snp.right)
            make.top.equalTo(dinnerLabel.snp.top)
            make.width.equalTo(100)
        }
        contentView.addSubview(dinnerDescLabel)
        dinnerDescLabel.snp.makeConstraints { make in
            make.top.equalTo(dinnerLabel.snp.bottom)
            make.left.equalToSuperview().inset(15)
        }

        contentView.addSubview(targetButton)
        targetButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(15)
            make.height.equalTo(80)
            make.width.equalTo(200)
        }
    }

    @objc func openMenu() {
        let menuVc = MenuViewController()
        navigationController?.pushViewController(menuVc, animated: true)
        menuVc.setType(type: 0)
    }

    lazy var targetButton: UIButton = {
        let view = UIButton()
        view.setTitle("Target", for: .normal)
        view.backgroundColor = .systemRed
        view.cornerRadius = 40
        view.rx.tap.asDriver().drive(onNext: { [weak self] () in
            self?.setTarget()
        }).disposed(by: rx.disposeBag)
        return view
    }()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let defaults = UserDefaults.standard
        let decoder = JSONDecoder()
        HTTP.GET(AppDelegate.baseUrl + "system/userFood/list?userId=" + (defaults.string(forKey: "id") ?? "")) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseUserFood.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        resp.data?.forEach { userFood in
                            if userFood.type == 1 {
                                self.breakfastButton.isEnabled = false
                                self.breakfastButton.titleForNormal = userFood.total?.string ?? ""
                                self.breakfastDescLabel.text = userFood.info
                            } else if userFood.type == 2 {
                                self.launchButton.isEnabled = false
                                self.launchButton.titleForNormal = userFood.total?.string ?? ""
                                self.launchDescLabel.text = userFood.info
                            } else if userFood.type == 3 {
                                self.dinnerButton.isEnabled = false
                                self.dinnerButton.titleForNormal = userFood.total?.string ?? ""
                                self.dinnerDescLabel.text = userFood.info
                            }
                        }
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
        getInfo()
    }

    func getInfo() {
        let decoder = JSONDecoder()
        let defaults = UserDefaults.standard
        HTTP.GET(AppDelegate.baseUrl + "system/user/getInfo?id=" + (defaults.string(forKey: "id") ?? "")) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseUser.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        self.targetButton.titleForNormal = "Target: " + (resp.data?.target?.string ?? "")
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }

    func setTarget() {
        // 1.
        var targetTextField: UITextField?

        // 2.
        let alertController = UIAlertController(
            title: "Set Target",
            message: "Please enter info",
            preferredStyle: UIAlertController.Style.alert)

        // 3.
        let loginAction = UIAlertAction(
            title: "Confirm", style: UIAlertAction.Style.default) {
                (_) -> Void in

                if let target = targetTextField?.text, target != "" {
                    let decoder = JSONDecoder()
                    let defaults = UserDefaults.standard
                    HTTP.PUT(AppDelegate.baseUrl + "system/user/update", parameters: ["id": defaults.string(forKey: "id") ?? "", "target": target], requestSerializer: JSONParameterSerializer()) { response in
                        if let error = response.error {
                            print("got an error: \(error)")
                            return
                        }
                        do {
                            let resp = try decoder.decode(ResponseInt.self, from: response.data)
                            print("completed: \(resp.code ?? -100)")
                            DispatchQueue.main.async {
                                if resp.code == 0 {
                                    self.getInfo()
                                } else {
                                    self.showToast(message: resp.msg ?? "unknown")
                                }
                            }
                        } catch {
                            print("decode json error: \(error)")
                            DispatchQueue.main.async {
                                self.showToast(message: "decode json error: \(error)")
                            }
                        }
                    }
                } else {
                    self.showToast(message: "please input info")
                    return
                }
        }

        // 4.
        alertController.addTextField {
            (txtPassword) -> Void in
            targetTextField = txtPassword
            targetTextField!.keyboardType = .namePhonePad
            targetTextField!.placeholder = "target"
        }

        // 5.
        alertController.addAction(loginAction)
        present(alertController, animated: true, completion: nil)
    }
}
