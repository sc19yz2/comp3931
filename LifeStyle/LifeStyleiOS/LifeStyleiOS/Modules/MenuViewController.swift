//
//  MenuViewController.swift
//  LifeStyleiOS
//

import SwiftHTTP
import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    lazy var contentView: UIView = {
        let view = UIView()
        self.view.addSubview(view)
        self.view.backgroundColor = .white
        view.snp.makeConstraints { make in
            make.edges.equalTo(self.view.safeAreaLayoutGuide)
        }
        return view
    }()
    
    lazy var tableView: UITableView = {
        let view = UITableView(frame: CGRect(), style: .plain)
        return view
    }()

    var dataList: NSMutableArray = []
    let selectedList: NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Menu"
        
        contentView.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CommonCell")
        tableView.tableFooterView = UIView()
    }
    
    var type: Int = 0
    func setType(type: Int) {
        self.type = type
        if self.type == 0 {
            let item = UIBarButtonItem(title: "add", style: .plain, target: self, action: #selector(add))
            navigationItem.rightBarButtonItem = item
        } else {
            let item = UIBarButtonItem(title: "done", style: .plain, target: self, action: #selector(done))
            navigationItem.rightBarButtonItem = item
        }
        getList()
    }
    
    @objc func done() {
        var info = ""
        var total = 0
        selectedList.forEach { item in
            let food = item as! Food
            info += ((food.name ?? "") + " ")
            total += food.calorie ?? 0
        }
        let decoder = JSONDecoder()
        let defaults = UserDefaults.standard
        HTTP.POST(AppDelegate.baseUrl + "system/userFood/create", parameters: ["info": info, "type": type, "total": total, "userId": defaults.string(forKey: "id") ?? ""], requestSerializer: JSONParameterSerializer()) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(ResponseInt.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }
    
    @objc func add() {
        // 1.
        var nameTextField: UITextField?
        var calorieTextField: UITextField?
        
        // 2.
        let alertController = UIAlertController(
            title: "Add Food",
            message: "Please enter info",
            preferredStyle: UIAlertController.Style.alert)
        
        // 3.
        let loginAction = UIAlertAction(
            title: "Add", style: UIAlertAction.Style.default) {
                (_) -> Void in
            
                if let name = nameTextField?.text, name != "", let calorie = calorieTextField?.text, calorie != "" {
                    self.addFood(name: name, calorie: calorie)
                } else {
                    self.showToast(message: "please input info")
                    return
                }
        }
        
        // 4.
        alertController.addTextField {
            (txtUsername) -> Void in
            nameTextField = txtUsername
            nameTextField!.placeholder = "name"
        }
        
        alertController.addTextField {
            (txtPassword) -> Void in
            calorieTextField = txtPassword
            calorieTextField!.keyboardType = .namePhonePad
            calorieTextField!.placeholder = "calorie"
        }
        
        // 5.
        alertController.addAction(loginAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func addFood(name: String, calorie: String) {
        let decoder = JSONDecoder()
        HTTP.POST(AppDelegate.baseUrl + "system/food/create", parameters: ["name": name, "calorie": Int(calorie) ?? 10], requestSerializer: JSONParameterSerializer()) { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(ResponseInt.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        self.getList()
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }
    
    func getList() {
        let decoder = JSONDecoder()
        HTTP.GET(AppDelegate.baseUrl + "system/food/list") { response in
            if let error = response.error {
                print("got an error: \(error)")
                return
            }
            do {
                let resp = try decoder.decode(BaseResponseFood.self, from: response.data)
                print("completed: \(resp.code ?? -100)")
                DispatchQueue.main.async {
                    if resp.code == 0 {
                        self.dataList.removeAllObjects()
                        let data = resp.data ?? []
                        for index in 0 ..< data.count {
                            print("id:" + (data[index].id?.string ?? "==="))
                            self.dataList.add(Food(id: data[index].id, name: data[index].name, calorie: data[index].calorie))
                        }
                        self.tableView.reloadData()
                    } else {
                        self.showToast(message: resp.msg ?? "unknown")
                    }
                }
            } catch {
                print("decode json error: \(error)")
                DispatchQueue.main.async {
                    self.showToast(message: "decode json error: \(error)")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommonCell", for: indexPath)
        cell.removeSubviews()
        let label = UILabel()
        label.font = label.font.withSize(14)
        cell.addSubview(label)
        label.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(10)
            make.right.equalToSuperview().inset(10)
        }
        
        let label2 = UILabel()
        label2.font = label.font.withSize(12)
        cell.addSubview(label2)
        label2.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(10)
            make.top.equalTo(label.snp.bottom)
            make.right.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(10)
        }
        
        let food = dataList[indexPath.row] as? Food
        label.text = food?.name
        label2.text = "Calorie: " + (food?.calorie?.string ?? "") + " KCAL/100g"
        
        if type > 0 {
            var selected = false
            selectedList.forEach { item in
                let id = (item as! Food).id
                if id == food?.id {
                    selected = true
                }
            }
            if selected {
                cell.backgroundColor = .red
            } else {
                cell.backgroundColor = .clear
            }
        }
        
        let view = UIView()
        cell.addSubview(view)
        view.backgroundColor = .gray
        view.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(0.5)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(dataList[indexPath.row])
        if type > 0 {
            let food = dataList[indexPath.row] as! Food
            var selectedIndex = -1
            for (index, item) in selectedList.enumerated() {
                let id = (item as! Food).id
                if food.id == id {
                    selectedIndex = index
                }
            }
            if selectedIndex >= 0 {
                selectedList.removeObject(at: selectedIndex)
            } else {
                selectedList.add(food)
            }
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = .zero
        }
        if cell.responds(to: #selector(setter: UITableViewCell.layoutMargins)) {
            cell.layoutMargins = .zero
        }
    }
}
