//
//  User.swift
//  LifeStyleiOS
//

import Foundation

struct BaseResponseUser: Codable {
    var code: Int?
    var msg: String?
    var data: User?
}

struct User: Codable {
    var id: Int?
    var name: String?
    var target: Int?
    var weight: String?
    var height: String?
    var age: Int?
    var gender: Int?
    var type0: Int?
    var type1: Int
}
