//
//  ResponseLong.swift
//  LifeStyleiOS
//

import Foundation

struct ResponseInt: Codable {
    var code: Int?
    var msg: String?
    var data: Int?
}
