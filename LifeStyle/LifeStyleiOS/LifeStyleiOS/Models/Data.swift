//
//  Data.swift
//  LifeStyleiOS
//

import Foundation

struct BaseResponseData: Codable {
    var code: Int?
    var msg: String?
    var data: Data?
}

struct Data: Codable {
    var target: Int?
    var distance: Int?
    var calorie: Int?
}
