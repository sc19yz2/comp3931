//
//  Food.swift
//  LifeStyleiOS
//

struct BaseResponseFood: Codable {
    var code: Int?
    var msg: String?
    var data: [Food]?
}

struct Food: Codable {
    var id: Int?
    var name: String?
    var calorie: Int?
    var selected: Bool?
}
