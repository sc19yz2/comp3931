//
//  Video.swift
//  LifeStyleiOS
//

struct BaseResponseVideo: Codable {
    var code: Int?
    var msg: String?
    var data: [Video]?
}

struct Video: Codable {
    var id: Int?
    var name: String?
    var url: String?
}
