//
//  Record.swift
//  LifeStyleiOS
//

import Foundation

struct BaseResponseRecord: Codable {
    var code: Int?
    var msg: String?
    var data: [Record]?
}

struct Record: Codable {
    var id: Int?
    var locations: String?
    var duration: String?
    var createTime: Int?
    var distance: Float?
}
