//
//  UserFood.swift
//  LifeStyleiOS
//

struct BaseResponseUserFood: Codable {
    var code: Int?
    var msg: String?
    var data: [UserFood]?
}

struct UserFood: Codable {
    var info: String?
    var type: Int?
    var total: Int?
}
